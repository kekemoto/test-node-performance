function notPromise() {
    for(let i=0; i<500000000; i++){var a = 1}
    console.log('notPromise')
}

var process = function (tag) {
    return new Promise(resolve => {
        for(let i=0; i<500000000; i++){var a = 1}
        console.log('process ' + tag)
        tag++
        resolve(tag)
    })
}

process(100).then(process).then(console.log)
process(200).then(process).then(console.log)
process(300).then(process).then(console.log)
notPromise()

